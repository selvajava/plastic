import React, { useState, useCallback } from "react";
import ImageViewer from "react-simple-image-viewer";

export const Gallery = props => {
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);

  const data = [
    { thumb:  "http://shreedurgasyntex.com/wp-content/uploads/2019/06/Super-Bright-chips.jpg", title:"Super Bright Chips" },
    { thumb: "http://shreedurgasyntex.com/wp-content/uploads/2019/02/Semi-Dull-Chips.jpg", title: "Semi Dual Chips" },
    { thumb: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUSEhIVFRUWDxAVFRUVFRUWFRcVFREWFhcVFRUYHSggGBolHRUVITEhJSkrLi4uFx80OTQsOCgtLisBCgoKDg0OFxAQGy0lICYtLS4tKy0rLS8wLS4tLS0tLS0tLS0rKy0uLS0tLS0tLS0tLS0rLS4tLS0tKy0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAgMBBAUGB//EADsQAAIBAgIGBwYFBAMBAQAAAAABAgMRBCEFEjFBUWEGE3GBkaHRIjJSscHwQmKC4fEjcpLSorLCcxb/xAAbAQEAAwEBAQEAAAAAAAAAAAAAAQUGBAIDB//EADQRAAIBAQQHBwQCAgMAAAAAAAABAgMEBRExEiFhgZGh0RMyQVFxscEUIuHwQvFDUjNygv/aAAwDAQACEQMRAD8AwADZH5wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASRiAeg0R0Vr1rSn/AE4cZK7fYvWx67RvR/DUbNQvL4pZvu3LuOCveNGlqX3PZ1y+dha2a6LRWWk/tXm89yz44ep4DB6HxFXOFJyXG2X+Ww69DobVfvzjHsu36eZ71oraKypelZ91Jc/foXNK47PHvty34Llr5s8tQ6GUvxVJS7Eo/O5tR6H4XhJ/q9Ed9Ekc7ttof837ex2Ru+yxWCpx4Y++LPPy6IYXhJfq/Y16vQ2j+Gc122foepMMhWyuv5viS7vsr/xx4dMDwtfoXUXuVFL9Lj6nIxWhsRT96m7cb3XjuPpkimSOmF51o97B7sPbVyOOrctml3cY+jx98XzR8pB9E0hoihV96CT+JZPx3955jSXRqrC8qftx5bV3b+4sqF40qmp/a9uXH+imtNz16Kxj9y2Z8OmJwgYZk7ypAABIAAAAAAAAAAAAAAAANrR2AnWlqx2filuS+9x5nNQTlJ4JHqEJTkoxWLeSIYLBzqyUIK7fglxvuR7rQWgaVG0nadT4nsj/AGrd27Ro7Bwox1YLte9vizp0pmetdvlVxjDVHm/Xoa2wXXCz4TnrnyXpt28MPHciTRTBlqaK4tyRCSMuRFkgGdYiAQS1jDZgjIAxKxVJE2VuSJBFmbGDLBBxtM6Ep1ryXsz+Jb/7lv7dp4rG4KdKWrNWflbjfej6VJnP0lg4Vo6sl2PenxRYWS3SpfbLXH29OnDbVW+7IWjGcNU+T9evHFHz4Gzj8FOjPVl3PdbijWNDGSktKORkpwlCTjJYNAAEnkAAAAAAAAAAAEN4F+Cwsqs1CO179yW+57jA4OFKChHve9vizU0JgOqhmvblZy+i7vmdNGct9rdaWjHurnt6fk2N2WD6eGnNfe+S8uvTPKL6RSjKZXlqb0KtyxM0qcjapyBJemGzEVxJOaAI2MqJCVRGFWvsTALNUi4kHN8PMw5vh5ggxJFTRNz5ENZcbdpIMMi5EpFUmAxN3K2GzDZJBo6TwUa0HF7dsXw/Y8RXpShJxkrNOzPoEjhdI8DrR6xLOK8V+xZXfatCXZyyfJ/nJ7Smvaw9tDtYd6PNfjw2avI8wADQGUAAIAAAAAAAOt0ewetPXfuws+1v0OSew0VQ6unFb7fP7S7jhvGv2dHBZy1bvH92lrc9n7a0aTyjr3+HXcdGBbFlCZO5mjYFlzKZWmSiwC+JsQqWNRE4zBJup8SWtfZ4v6I03VXby9TLm3tdgDZlKK3+JB10UXjuV+bzMOo+NvAAv67l8jDrP4fkVda/iRh1HyZILXUXZ2h2f7lKktzsYb4+RBBJ3RBsaz7URcrkgMg2YbDZJBCTKpq+0sZCTBB4rSWG6upKO69137DWPQ9JKF1Ga/C/nsPPGpslbtaSk8/H1MReFn+ntEoLLNej6PFbgADoOMAAAAAA2tG0tepFbs79lj10Ged0BHOUuCXm2d+LM9ek9Kto+S99ftga65KWjZtL/Zt8NXunxL4saxUmZ1itLguTLYs14ssciAXRY19y739Cpz9ApW+gBfrWK62JjHOTNPHY1U4tt52PE47SdStJqDy3vd98j3GLk0ksX5Hmc1CLlJ4JZtnp9JdJ4QyT++w4VTpLWl7qf6U/oc6lhorN+0+Mv/MS0t6V1SaxnLDYuuXJlDXv6CeFKGO1vDlnxwew2lp/E/DPwkbFDpZOL9qLX90TnGJJPJq/9x9J3VHD7ZPHal+D4Qv6WP301hsbXvj8HtdG6cpVtjSlw2P9zpxqcfE+VYyHVrrI3VmrpbNu2Pkel6NdI9e1Oo83slx5PmVNehOjPRkX1ltMLRT7Snllg/BnsGyLZCMjNz4nQSuQuDEiSDDK5FjZVIEGrj6evTlHisu1Zo8cj28jxuJp2lJcG/IubqnqnH0fw/gzt/U/+Op6r5XyVAAuDPAAAAAxcEpYna0GrRb5ryudZTOLoqpaDf5joKtw5W77+hl7am7RN7fhG6u6DVlprZ+Td1xGRqyq2WzO9tuWziZVXblsV/n6HJgzswN6nIl1m81FPK/IksQrptcPNXCTYwNqIqVEs9yNZV/a5Xt5J/VGhpfG6sJ8le/0J0WMGcLpBpCVSfVxe/P75L6mvSpqKsv5/MaGjq6dSV/ecW32Xz+h0dYv7roKNPtPF8ljkZa/K05VlR/jHDe347sltTMgxcxrfP8A86xaFHoMkDClnb8tzCnt/V/xBPZy8v39RTpJf0qn/wA5nA0fXadvA7GmK2rSfGVo+PtS+/zHnqG0or01zS8l1Zq7gpyVnlJ5OWrgl7+x9X6PaS66krv2ouz58H3nZufP+h+Kcaur8cWtts1n6nuVW5bo78s77+4qYpsuWi9sxcpVbZl8W/LJrf3h1eW9Lbxt6nrRZ5aZayEiM552Svlf78CudTlkrX+ezvCiyMCUjyWlFarPt+eZ62R4/SU71ZO29fMtLqxdSXp8opL8WNGH/b4ZQDDkZLwy7i1mAACAzGrt/V5mQD0pNZHS0XH2e6Pk8vkb6htz4d1v5OZouWcl2PwOkmZi8NKNonu5pG2uqppWSnvXBtE1Hms3wy2cCSp7k91nl27OG0imTucemywxZKV7ZO3mThD5ryjYrTLISIUmB1W1Xyu/ONrHE6TK1Lbtyf8Alf6nde04/SOjek+WZOnLzJTZ4N13Tmprd5rej0OGrRnHWi7p+KlwZ53EwuU4fEzpO8XbivwvtRY2G3OktGWtFZed3/VJSg8Jrnse/J+Gvd62xhx5fj4fl1Tm4bTdN++pRfjH/Y21j6fxx8X6F3G1UpLFSXHD3wZl52O103g6ctyxXFYrmXyh9Pr/ALGJ2Sk27JqX/I062laUfil4KP8Ak/Q4+N0lKplsXCOw+Ne8KdNPReL2fL/s67LdVqrSWn9kfN57lnvawGlcZ1kuS9lf7dpRh45lMInQw9Kxn6taUm23ma+lTjSgoQWCWR2uja/r0+2X/Vn0KNO+z8u7LJv1PDdFKF6zlujF+Ly9T3tFZHxjJiT1lbo81v3ZZ22K+WwdVnffdO9uCS8PUsYR602ecWQlDO6dsrbL/e0rnT3Xye3wtt7i5lciVJoEHf77f4PHYrOTz3/U9biJ6sXLgvoePLe6Y9+XovnoZ2/quCpwW18MMPdmHEyAXRnG2wACCAAAC/BTtNLt+X8nWTOEdihU1op8yivelrjUXp8r54GnuCvjGdF+Gtb9T+OJsxZNFSZNFMaEmmSuQZKm9wBcs0U4undWexqxZHIslG6APm+PwzhOUHufkc+rRPfad0T1sdaPvx2c1wZ4+VPNpqzW1MlPA9nKdIx1Z05Ye5W8KetMGh1ZOnRbN6OGLo0iHMGvRoWNqMbElCyuz0fR3Qjk1VqKyWcIv/szzmHqOn0a0d1dNay9qXtS5cF98Wd9RFKnYmz0fNkZGImXmZZJBBlTLJlUgScrT1a0FHfL6Zs86b2l8Tr1HbYsl6miaiw0eyopPN63v/BiL0tHbWmTWS1LdnxeO7AAA6yvAAAAAABuYCrZ6r35LtNMymfG0UVWpuD8ffwOiyWmVnrRqx8PDzXijtJlkGamHray57+w2EzIzhKEnGWaN9TqRqwU4PFPI2IoxJGKUy9SW/xPJ6JU532lsYfzu/YodJrNbC6lPuf3tBJfqp5M5WlNA06ubyluktvfxOxTlxLlFEkHz/F9H8RDZHrFxjt70znzoyj70Jx7Ys+oqi/vMKlyIwJxPl0YX2KT7Is38JoXE1PdpOK+KeXltPokaK4FiiTgNI81ovoxTptSqPrJ817K7EeghSsXqHIy0iTyVJFcmWTZFIAikYkTkymUgCEmc/S2L6uFl70tn1NytWUU5Sdkldnk8dinUm5PuXA7rBZu2qYvurP4XXYVl6W36elhHvSy2LxfTbsxNYAGnMUAAQSAAAAAAAAATpVXF3X2zq05qSujjl2HruL5b197iut9h7eOlDvLmvLo93kW913l9NLQn3Hyfn1W/wBevEvpzNWlUUldFqZmmnF4PM2MZKSUovFPJm7SfDwL7Re1WfFGpSkbdOXEgloup02tmZfFMqhyLYx+7kkF0Yk0iEE+LLEiQZ7wY7CDir5/MkgsckRZmP3/ACZAK3FFcuZZOZrVW3tAIzmVTqJK7dktrIVqyinKTslvPN6T0k6mSyhw48zps1lnXlgtS8X+5vYjittup2WGMtcnkvPotrJaW0j1jtH3E8ub5nNANPSpRpQUIZGKr1515upUeLf7gtiAAPofIAAAAAAAAAAAAAAAnTqOLun98jp4bFxlk8nw4HJByWmxU7Qvuz81n+TvsV41rK/t1x/1eX4fpvTPSQNqlLg/E83h8dOGx3XDd3nTw+k4PbeL4LPzKKvdtelrS0l5r5WfutpqLNe9mr4JvRfk/h5Pk9h2qc+KNiDOfQqJ+6/Bm5TkcGwss9ZsxLUUJmdckguMSqFWsvtldQEFjrEHVZRUqRirylbne3zNDEabox2Xm/BeLPrTpTqvCCb/AHzyPlWr06KxqSS9X8Z8jqaxoaQ0pTp5e9L4f33HCxmmatTK+rHgvrvOaW1nurxqvcvl9OJRWu/V3bOv/T+F1w9DYxmNnVd5PsS3diNcAuYQjBaMVgjO1KkqknKbxb8WAAejwAAAAAAAAAAAAAAAAAAAAAAAAZjJrY7dpt09J1o7KnyNMHidOE+8k/VJ+59KdapT7kmvRtex1I6erLen2qP0Lf8A9FV+FeBxgfB2Gzv+C4HUrzta/wAj/fU6z6QV+KXd6o16ulq8ttT5I0Qe42WhHKC4I8Tt1pnnUlxZmUm8222YAPucjzxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/9k="
    , title: "Fully Drawn Yarn(Bright)" },
    { thumb: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIUExYQExMPFxQSDxYSFxMTEA8PGBMQFxYXFxYTFhYZHykhGRsmHBgUIjIiJiosLy8vGCA1OjUtOSkuLywBCgoKDg0OHBAQGy4mHyYwLjUwLCwuLi4sLi4sLi4wLjAsLi4uLi4uNy4sLi4wMCwuLi4uLi4uLjAuLi4uLi4uL//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwUCBAYBB//EADcQAAIBAgMFBgQFAwUAAAAAAAABAgMRBCExBRJBUXEGImGBkaETMsHRI0JSYrHh8PEUM3Ki0v/EABsBAQACAwEBAAAAAAAAAAAAAAAEBQECBgMH/8QANBEAAgECAgcHAwMFAQAAAAAAAAECAxEEIQUSMUFRYbETcYGRocHRMuHwFCLxNEJDgpIV/9oADAMBAAIRAxEAPwDwAHZHzgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA29n7OqVnaKyWsnw+78DosNseFPheX6tfTkRq2KhSyeb4fmzryJ2F0fVxGayjxftx6czl4Yab0j9DydGS1R108Oa9TCrkRP18r7EWn/i07fW7+HT7nKgva+zovhf3K+ts5rTPorEqGMpy25ECtoqvDONpLlt8n7NmkDOdKS1XtcwJSd1dFbKLi7SVnwYALLD7GqyV7WT5tLLoaTqRgrydj0pUalV2ppt8itMoQbySb9i8pbH3fmTfisvc36WFS4L0Ic8fFfSr9C1o6Gm86krclm/PZ1KGjsyb1yNmOxlzL2FAmhRIcsZVe+3cWUNGYaKtq37/AM6HK4nZNSOaW8vDIrj6DGgV21Oz/wAROcLKfLTe/qSKGkM7VPP5IWL0RlrUP+fh+z8zjwZVKbi3GSaadmnk0zEsygasAAAAAAAAAAAAAAAC32DsaVeV3dU4vvPm+S8f4NfYuzJYiqqcclrKXKK1fXgvFn07D4KFOCpwVoxVkvq+bK/HYzslqR+p+n5uLbRmj+3faT+her+Fv8uJXwwkYRUIRSitEjCVEtJUiKVIo9bidQkkrFXOgQTolrKma9enZG2sLFXKgexoRXBEzR6HNmVFGG6VG19lRacoJKSzaWjXHIu7kUmKVeVKWtE0r4eFeGpNZdOa/O85XYOHU6ib0ijroRKPZlJQrVEtMrdLJ/UvoyJGOq69W62WVvFX9yJoyj2VDVe28r96bXsZKJ5/p+KPYs2qBGUicQQok9PD3N6NBamagjGuLGrGh4EsaaJm+Ri0YuDnO1OxVVXxaa/Eis0vzRXDquHocKfXnA4jtjsbcl8eC7s3aSX5ZPj0/viWuj8V/il4fHx5d1FpfA3Xbw/2Xv8APnuOYABcHOgAAAAAAAAAIHQ9idnKpXU5fLS778ZP5V65+R51qkacHOWxHrQoyrVFTjtf5fwOs7MbL/09LvL8SdnP6R8v5bLlErgmRuBys5ynJyltZ3NOnGnBQjsRkRypo9TE6iim3wNTc08ZLdXi9PuaUZXyZLiJb/e48vA1b2MggqrMxJK5qzmDJlUkiHfIqlUw3jDNjXpTtWl4xXtYtqdQ5741q1uf2uW8ZntXi1qvjFP0+xHw81LXS3Skvf3LGMjcwepRvGqPzNLq7Fts+snmjyPYtZzPY5mtF3Zv06aSBgx3Qz0wlNaAHrVjU2hRjUhKnLScbP7+tmTNtmLQTazRhpNWZ8rxeHlTnKnLWMrP7kJ1vbfAW3a6X7ZddYv+V5I5I6nD1u2pqfn3/mfcziMZh/09Z0927uez470AAexGAAAAB5cGUrnp9G7J4H4dCLa70+++j0X8ep88wlPfnGCv3qkYZfuaV11ufW1UiklFZKMd23FO6S9iq0rN6sYLfn5fz6F9oPDvXlUe7Lzzft5ksJtE0KqZqyqW1Vm3bN2Wl73MVU1ya3Y71vXL2KVJnR2N5xuVO1Z57i4ZvqbjxO7He5K/9ChqYq/ed82m/NXuEmxYzhUaMpZq5q1K2en5t32vcjr4yMFK+kFdv6IzqsWMNoYxQXNvRc/sjnMbtjd1bb5LJeXF9SPae0HZzes+H6Vwp/VnNzqOTbZiV0bRjc6bZ21lUlu2aeq8S4Whxey/96n/AM176nYVp7sb8nbXwuaJSlkkbtJbSkrVfxXLx/wdJRzOP3s89bXOo2Ri1KnFvVJ36R0fmrFzpLDuMINbsvTLozntDV3UqVE/7v3ebd/byKHtBJ/GaztGKt5q5tdndsSpzUZvuvLoedqKDjVjNqynT65xefs0UzehT5nQWyPruEatfwuuhlVxDZynZnazlSVN5yg7a27tn9i7+NbVZ93jzbWpsk2aWZZUpXJLGlQxGaXNtapK68fM2ZV0uGlru61dvXUzqsxZkm6LJEdSu72Su7X1tkRurq7OySb4Wyvp0FmLEO2MP8WlOlb5l7rOPukfLz68qZ8y7R0NzETSWTkpLo1f6teRa6KnnKHj7euRQ6boayhVW3Z55ro/FlcDHeMi6OcasAAYMBmO7r439zIA2Umthbdk8MpYmm+Eb+Pyq69z6IqObadrtNeDTb9HdnCdjZWqyf7H9Dt4VSg0lN9vbkvk6zQ/9Nfi37L2J40tc45u7W73XlbS/wBTxUGlZS1juvK+V3pnlq/Y9jUMt4gazLW7NLaydlGLt4NXy4FU6NuOjXsrfU2cdVvN+GXoak5GLswYqllrld+m7axTbXlmoXuoxU5eLu7J9Wy3qTsvI5faOIynL97/AOqsv5M67M3ZR7SrXk83k+ufFmrBHkpGVM8ZTbPZFz2ew29WTekE5PLyS9/YutuzShuK3ekna3Jf4I9hYf4dPea71TvPwj+VfXzNDaOI35t8Fp05lho2m51k90c/j16MqtL4nssO4rbLJd2/0y8TTlH+/X/0WuwppScG8pJr7lYZU5tNNa3uX9en2tNw49d3qcthcXKhWjU4be7f6bOZ1O3MG6tBu95U++kla6XzL0/g5bDYKU81onm9c7aHcbLrKUFNcVf7oodpN4ebio3ozvKm1k4t5uF+vtbkcpdp2Z3SldftK7AXo1oNtOLlZ9NM14XO1cb5pr8vC67rbPnlas5S3n/hckd3snE71KN9dxZ+KMqT3GGbsaTyd1q3nHLO2iv4G/ToX72W9k07aNZW6amtSszew9lxM6zMXZk6LvdNJ2tmr5cOIlS1V8pK0rq7eVr34ZWPZ1SCpXFzXWZNKpbirfW/2OE7a0u/CV1nDd0t8ufM6ydU5ntdnGm+TkvVL7EzAS1a8ed+jK/SeeFnbdZ+TRzDj/Fj0A6M49ybVjwHoMGAAAC37MVbVkv1Ra9EdlGZ88wVfcnGf6Wn5cV6Hd0qqaTTyauuhQ6Uhaqp8V6r7NHU6Eqa1Bw3p+j+6ZvQqk3xv4NBSFWfdfRlbcuDUlUv5sgnUPHIgcjBskZVpnMbVX4clxU5X6OzX19DoqjNLEYVT145PoYNrHFlxsHZnxGqkl+HF8fztcOnP05ljR2DRTu9+X7W1b2RZVaijHgklosrJcDTVN3I19qYmystdfPkUhLiarlJsiOqwWH7ClZ7Xm/jw63OG0ljP1NbWX0rJd3Hx6WW4AAlkAudh49x/Cbyv3fBl5iKUKsHTmrp6c4y5p8GcWnbM6PZ2M3o34rJrwKHSeG1ZdrHY9vf9+vedTobGa8OwltWzu+3TuKfE7BrRlZbsot/Nezt4rmdFgIbsVHlGxI6lzGJVl4zdpzNujXd/IrYSNmEjKNGjelVIpTI3MjczY0JJTOd7U1MoLxb9EvuXU6hy+38RvTstIO3rm/oTcBHWrrld+lvcr9Kz1MLJcbL1v0TKsAHRHIAAAAAAAvdibV3Uqc3kvlk8yiB416Ea0NSX8Mk4TFTw1TtIeK3NcDvIVjKrLuvozjsHtKpT0d4/p4F/gtpwqK17P8ATr6cznsRgqtDN5x4r3W7pzOtwmkaOJ/aspcH7Pf15HrkRykezfAjlIh3LE8kzFMSZjvmLgklKxTY/E7zsuHuvuSY3GX7sX1fL++RXl7o/BOL7Wos9y4c3z4cNpzOltJqSdCk8t748l7vfs2XuABbnPAAAAnwtdwldea5kANZwU4uMtjN6dSVOSnF2a2M6elWUkmnqTxZzeDxTg/Bl3h6yaumczi8LLDyz+nc/nn12rl22Bx0MVDLKS2r3XLp13oMljI1IzJN+xEJtrmy6hDUrFdi9qRjks3y09ymxWNnPV5cidh8DVrZ7Fxfst/TmVmL0lQw/wC2+tLgvd7F6vkWuO2qkmou8ihbvm+J4C9w+FhQjaPizmMZjqmKknPJLYlu/OIABIIYAAAAAAAAACYABZYfaT0nn48f6m58VPRlCeFbW0XSqO8Xq92zyLnDabrUo6s1rd7s/PO/jnzLetiorj5GlXxTllouWtzWB6UNH0qT1tr4v2/L8zxxWlq9dav0x4Lf3v4snvQABOKwAAAAAAAAAE2Hryg7p+RCDEoqS1ZK6NoTlCSlF2a3otKe1ucX6kOL2jOeS7q5ap+ZogiwwNCEtZRz8X1bJtXSmKqQ1JTy5JJvvaSAAJhAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//2Q==", title: "Fully Drawn Yarn(Semi dual)" },
    { thumb: "http://shreedurgasyntex.com/wp-content/uploads/2019/05/2.jpg", title: "Bedsheets" },
    { thumb: "http://shreedurgasyntex.com/wp-content/uploads/2019/05/1.jpg", title: "Bedcovers" }
  ];

  const images = data.map(obj => obj.thumb.replace("-small", "-large"));

  const openImageViewer = useCallback(index => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const closeImageViewer = () => {
    setCurrentImage(0);
    setIsViewerOpen(false);
  };

  return (
    <div id="portfolio" className="text-center">
      <div className="container">
        <div className="section-title">
          <h2>Gallery</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit duis sed dapibus leonec.</p>
        </div>
        <div className="row">
          <div className="portfolio-items">
            {data.map(({ title, thumb }, index) => (
              <div key={index} onClick={() => openImageViewer(index)} className="col-sm-6 col-md-4 col-lg-4">
                <div className="portfolio-item cursor">
                  <div className="hover-bg">
                    <div className="hover-text">
                      <h4>{title}</h4>
                    </div>
                    <img src={thumb} className="img-responsive" alt="Project Title" />{" "}
                  </div>
                </div>
              </div>
            ))}
          </div>

          {isViewerOpen && (
            <ImageViewer
              src={images}
              backgroundStyle={{ zIndex: 99999 }}
              currentIndex={currentImage}
              onClose={closeImageViewer}
            />
          )}
        </div>
      </div>
    </div>
  );
};

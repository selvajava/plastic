export const Footer = (props) => {

  return (
    <div>
      <div id='footer'>
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="col-md-6 col-lg-3 about-footer">
                <div id="bunch_contact_us-2" class="footer-widget widget_bunch_contact_us">
                  <div class="contact-widget">
                    <h3 class="footer-title">Contact us</h3>				<div class="widget-content">
                      <ul class="contact-info">
                        <li><span class="icon-signs"></span>Block No. 128,129,130 &amp; 175, Plot No. Z &amp; E R.S. No. 120 &amp; 120/1 Taluka : Palsana, Village : Jolva Dist. : Surat - 394305.</li>
                        <li><span class="icon-phone-call"></span> Phone:  <a href="tel:+9102612239106">+91-02612239106</a><br />Factory Board Line : 09974093573, 74 - Ext 214</li>						<li><span class="icon-e-mail-envelope"></span>shreedurgasyntex@gmail.com</li>					</ul>
                    </div>
                  </div>

                </div>
              </div>
              <div class="col-md-6 col-lg-5 page-more-info">
                <div class="about-widget">
                  <h3 class="footer-title">Our Business</h3>
                  <div class="widget-content">
                    <div class="text"><p></p><p align="justify">Shree Durga Syntex Pvt. Ltd (SDSPL) is a leading manufacturing company in Textile industry producing Polyester Chips, Yarn &amp; Greige Fabric. It was incorporated in 2003 and initiated with a concept of catering from raw material to finished goods for man-made fibre.</p><p></p> </div>
                    <div class="link">
                      <a href="http://shreedurgasyntex.com/company-profile/" class="default_link">More About us <i class="fa fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>

              {/* <div class="col-md-6 col-lg-3 page-more-info">
<div class="footer-title">
<h4>More Info</h4>
</div>
<ul>
<li><a href="#">Lorem ipsum</a></li>
<li><a href="#">Dolor sit amet</a></li>
<li><a href="#">Consectetur Adipisicing </a></li>
<li><a href="#">Ed do eiusmod tempor incididunt</a></li>
</ul>
</div> */}
              <div class="col-md-6 col-lg-4 open-hours">
                <h3 class="footer-title">Open hours
                  {/* <h4></h4> */}
                </h3>
                <table class="table">
                  <tbody>
                    <tr>
                      <td><i class="fa fa-clock"></i>Monday Thursday</td>
                      <td>9:00am - 5:00pm</td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-clock"></i>Friday</td>
                      <td>9:00am - 4:00pm</td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-clock"></i>Sturday</td>
                      <td>9:00am - 1:30pm</td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-clock"></i>Sunday</td>
                      <td>9:30am - 12:00pm</td>
                    </tr>
                  </tbody>
                </table>

              </div>
            </div>
          </div>

        </div>
        <div className='container text-center'>
          <ul class="footer-social">
            <li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="" target="_blank"><i class="fa fa-linkedin"></i></a></li>

          </ul>
          <p>
            &copy; Copyrights © 2021 All Rights Reserved. Developed by  {' '}
            <a href='http://www.templatewire.com' rel='nofollow'>
              Mani
            </a>
          </p>
        </div>
      </div>
    </div>
  )
}
